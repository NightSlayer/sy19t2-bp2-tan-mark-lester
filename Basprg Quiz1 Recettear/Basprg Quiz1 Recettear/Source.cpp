#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

struct NPC
{
	string npcName;
	Item itemOrder;
};

Item buyingRandomItem(){
	int r = rand() % 10;
	Item item; 

	switch (r)
	{
	case 0:
		item.name = "Wooden Armband";
		item.gold = 23;
		break;
	case 1:
		item.name = "Leather Glove";
		item.gold = 33;
		break;
	case 2:
		item.name = "Wool Hat";
		item.gold = 28;
		break;
	case 3:
		item.name = "Focus Staff";
		item.gold = 55;
		break;
	case 4:
		item.name = "Wooden Shield";
		item.gold = 62;
		break;
	case 5:
		item.name = "Longsword";
		item.gold = 122;
		break;
	case 6:
		item.name = "Crafter�s Knife";
		item.gold = 272;
		break;
	case 7:
		item.name = "Longbow";
		item.gold = 186;
		break;
	case 8:
		item.name = "Willpower Ring";
		item.gold = 1083;
		break;
	case 9:
		item.name = "Knight�s Helm";
		item.gold = 1596;
		break;
	default:
		break;
	}

	return item;
}

NPC generateRandomNPC() {
	int r = rand() % 4;
	NPC randomNPC;
	
	switch (r) {
	 case 0:
		randomNPC.npcName = "Man";
		randomNPC.itemOrder = buyingRandomItem();
		break;
	 case 1:
		randomNPC.npcName = "Old Man";
		randomNPC.itemOrder = buyingRandomItem();
		break;
	 case 2:
		randomNPC.npcName = "Woman";
		randomNPC.itemOrder = buyingRandomItem();
		break;
	 case 3:
		randomNPC.npcName = "Girl";
		randomNPC.itemOrder = buyingRandomItem();
		break;
	 default:
		 break;
	
	}
	

	return randomNPC;
	//This function generates random NPC
	//Randomize from the 4 given names
	//Randomize an item to buy

}

void guildItem() {
	Item guildDataBase[10];
	guildDataBase[0] = { "Wooden Armband" , 15 };
	guildDataBase[1] = { "Leather Glove" , 20 };
	guildDataBase[2] = { "Wool Hat" , 25 };
	guildDataBase[3] = { "Focus Staff" , 30 };
	guildDataBase[4] = { "Wooden Shield" , 35 };
	guildDataBase[5] = { "Longsword" , 120 };
	guildDataBase[6] = {"Crafter�s Knife" , 140 };
	guildDataBase[7] = { "Longbow" , 140 };
	guildDataBase[8] = { "Willpower Ring" , 600 };
	guildDataBase[9] = { "Knight�s Helm" , 900 };
	cout << "Guild items :" << endl;
	cout << "==============" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << i+1<< ".)"<<guildDataBase[i].name << " | " << guildDataBase[i].gold << endl;
	}
	cout << "0 to exit" << endl;
}


int guildShop(int &gold, vector<Item>&inventory) {
	int input;
	bool willKeepBuying = true;
	while (willKeepBuying==true) {
		int size = inventory.size();
		cout << "Current Gold: " << gold << endl;
		cout << "INVENTORY" << endl;
		for (int i = 0; i < size; i++)
		{
			cout << inventory[i].name << endl;
		}
		guildItem();
		cin >> input;
		switch (input){
		  case 0:
			  willKeepBuying = false;
			  return gold;
			  break;
			  //go to shop
		  case 1:
			  if (gold > 15) {
				  inventory.push_back({ "Wooden Armband", 15 });
				  gold -= inventory[size].gold;
				  cout << "Added Wooden Armband" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
				break;
		  case 2:
			  if (gold > 20) {
			    inventory.push_back({ "Leather Glove" , 20 });
			    gold -= inventory[size].gold;
			  cout << "Added Leather Glove" << endl;
		      }
			  else {
			  cout << "Not enough gold" << endl;
			  system("cls");
			  guildShop(gold, inventory);
			  }
			  break;
		  case 3:
			  if (gold > 25) {
			  inventory.push_back({ "Wool Hat" , 25 });
			  gold -= inventory[size].gold;
			  cout << "Added Wool Hat" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 4:
			  if (gold > 30) {
			  inventory.push_back({ "Focus Staff" , 30 });
			  gold -= inventory[size].gold;
			  cout << "Added Focus Staff" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 5:
			  if (gold > 35) {
			  inventory.push_back({ "Wooden Shield" , 35 });
			  gold -= inventory[size].gold;
			  cout << "Added Wooden Shield" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 6:
			  if (gold > 120) {
			  inventory.push_back({ "Longsword" , 120 });
			  gold -= inventory[size].gold;
			  cout << "Added Longsword" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 7:
			  if (gold > 140) {
			  inventory.push_back({ "Crafter�s Knife" , 140 });
			  gold -= inventory[size].gold;
			  cout << "Added Crafter�s Knife" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 8:
			  if (gold > 140) {
			  inventory.push_back({ "Longbow" , 140 });
			  gold -= inventory[size].gold;
			  cout << "Added Longbow" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 9:
			  if (gold > 600) {
			  inventory.push_back({ "Willpower Ring" , 600 });
			  gold -= inventory[size].gold;
			  cout << "Added Willpower Ring" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl; \
					  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;
		  case 10:
			  if (gold > 900) {
			  inventory.push_back({ "Knight�s Helm" , 900 });
			  gold -= inventory[size].gold;
			  cout << "Added Knight�s Helm" << endl;
			  }
			  else {
				  cout << "Not enough gold" << endl;
				  system("cls");
				  guildShop(gold, inventory);
			  }
			  break;

		  default:
			   system("cls");
			  guildShop(gold, inventory);
             
			  break;
		}
		if (input == 0) {
			break;
		}
		system("pause");
		system("cls");

	}

}

int sell(int& gold, vector<Item>&inventory) {
	
	
	cout << "Begin Shop" << endl;
	for (int i = 0; i < 5; i++) {
		bool isInInventory = false;
		int index = 0;
		NPC randomNPC = generateRandomNPC();
		cout << "Current Gold: " << gold << endl;
		system("pause");
		cout << randomNPC.npcName << " is looking for " << randomNPC.itemOrder.name << endl;
		for (int i = 0; i < inventory.size(); i++) {
			if (randomNPC.itemOrder.name == inventory[i].name) {
				index = i;
				isInInventory = true;
				break;
			}
		}
		if (isInInventory == true) {
			cout << "Sold for " << randomNPC.itemOrder.gold << endl;
			gold += randomNPC.itemOrder.gold;
			inventory.erase(inventory.begin() + index);

		}
		else if (isInInventory == false) {
			cout << "That item isn't in your shop, customer left..." << endl;
		}
		system("pause");
		system("cls");
	}
	return gold;
}


int main() {
	srand(time(NULL));

	int gold = 1000;
	vector<Item> inventory;
	while (gold < 10000) {
		gold=guildShop(gold,inventory);
		gold = sell(gold,inventory);
		
	}
	system("pause");
	return 0;
}