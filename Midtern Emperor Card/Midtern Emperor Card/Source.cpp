#include <iostream>
#include <string>
#include <time.h>
#include <vector>


using namespace std;

void emperor(vector<string>& emperor) {
	emperor.push_back("Emperor");
	for (int i = 0; i < 4; i++) {
		emperor.push_back("Citizen");
	}
}

void slave(vector<string>& slave) {
	slave.push_back("Slave");
	for (int i = 0; i < 4; i++) {
		slave.push_back("Citizen");
	}
}

void EmperorOrSlave(vector<string>& playerCard, vector<string>& enemyCard, int& round, string& side) {
	if (round <= 3) {
		emperor(playerCard);
		slave(enemyCard);
		side = "Emperor";
	}
	else if (round <= 6) {
		emperor(enemyCard);
		slave(playerCard);
		side = "Slave";
	}
	else if (round <= 9) {
		emperor(playerCard);
		slave(enemyCard);
		side = "Emperor";
	}
	else if (round <= 12) {
		emperor(enemyCard);
		slave(playerCard);
		side = "Slave";
	}
}
void checker(vector<string>& playerCard, vector<string>& enemyCard, string& side, bool& win) {
	int PlayerAnswer;
	int EnemyAnswer;
	cout << "Pick a card to pick" << endl;
	for (int i = 0; i < playerCard.size(); i++) {
		cout << i + 1 << ".)" << playerCard[i] << endl;
	}
	cin >> PlayerAnswer;
	if (PlayerAnswer > playerCard.size()) {
		system("cls");
		checker(playerCard, enemyCard, side, win);
	}
	EnemyAnswer = rand() % enemyCard.size();
	if (side == "Emperor") {
		if (PlayerAnswer == 1) {
			if (EnemyAnswer == 0) {
				cout << "You picked Emperor" << endl;
				cout << "Enemy picked Slave" << endl;
				cout << "You Lose" << endl;
				win = false;
			}
			else {
				cout << "You picked Emperor" << endl;
				cout << "Enemy picked Citizen" << endl;
				cout << "You Win" << endl;
			}
		}
		else {
			if (EnemyAnswer == 0) {
				cout << "You picked Citizen" << endl;
				cout << "Enemy picked Slave" << endl;
				cout << "You Win" << endl;
			}
			else {
				cout << "You picked Citizen" << endl;
				cout << "Enemy picked Citizen" << endl;
				cout << "Draw" << endl;
				playerCard.pop_back();
				enemyCard.pop_back();
				system("pause");
				system("cls");
				checker(playerCard, enemyCard, side, win);
			}
		}
	}
	else if (side == "Slave") {
		if (PlayerAnswer == 1) {
			if (EnemyAnswer == 0) {
				cout << "You picked Slave" << endl;
				cout << "Enemy picked Emperor" << endl;
				cout << "You Win" << endl;
			}
			else {
				cout << "You picked Slave" << endl;
				cout << "Enemy picked Citizen" << endl;
				cout << "You Lose" << endl;
				win = false;
			}
		}
		else {
			if (EnemyAnswer == 0) {
				cout << "You picked Citizen" << endl;
				cout << "Enemy picked Emperor" << endl;
				cout << "You Lose" << endl;
			}
			else {
				cout << "You picked Citizen" << endl;
				cout << "Enemy picked Citizen" << endl;
				cout << "Draw" << endl;
				playerCard.pop_back();
				enemyCard.pop_back();
				system("pause");
				system("cls");
				checker(playerCard, enemyCard, side, win);
			}
		}
	}

}
void WinMoney(int& money, string& side, bool& win, int& bet, int& mm) {
	if (win == true) {
		if (side == "Emperor")
			money += 100000;
		else if (side == "Slave")
			money += 500000;
	}
	else if (win == false) {
		mm -= bet;
	}
}
void ending(int money, int mm) {
	if (money >= 2000000)
		cout << "You Managed to clear your debt" << endl;
	else if (money < 2000000 && mm > 0)
		cout << "You Didn't get enough money" << endl;
	else if(mm<=0)
		cout << "You Didn't get enough money and lost a ear" << endl;
}
void playround() {
	int money = 0;
	int mm = 30;
	int round = 1;
	int bet;
	string side;
	int PlayerAnswer;
	int EnemyAnswer;
	for (int i = 0; i < 12; i++) {
		bool win = true;
		vector<string> playerCard;
		vector<string> enemyCard;
		EmperorOrSlave(playerCard, enemyCard, round, side);
		cout << "Cash: " << money << endl;
		cout << "Distance Left: " << mm << endl;
		cout << "Round: " << round << "/ 12" << endl;
		cout << "Side: " << side << endl;
		cout << "How many mm do you bet: ";
		cin >> bet;
		system("cls");
		checker(playerCard, enemyCard, side, win);
		WinMoney(money, side, win, bet, mm);
		round++;
		if (mm <= 0||money<=2000000)
			break;
		system("pause");
		system("cls");
	}
	ending(money, mm);
}

int main() {
	srand(time(NULL));
	playround();
	system("pause");
	
}