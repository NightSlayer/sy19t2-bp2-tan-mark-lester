#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

int bet(int& gold, int &betting ) {
	betting = 0;
	cout << "How much are you betting? " << endl;
	cin >> betting;
	if (gold<betting||bet==0) {
		if (bet == 0) {
			cout << "You didn't bet anything" << endl;
		}
		else {
			cout << "You don't have that money to bet" << endl;
		}
		system("pause");
		system("cls");
		bet(gold, betting);
	}
	else {
		cout << "You betted " << betting << " gold" << endl;
		gold -= betting;
		system("pause");
		system("cls");
	}
	return gold;
}

int diceRoll() {
	int dice1 = rand() % 6 +1;
	int dice2 = rand() % 6 + 1;
	int totalsum = dice1 + dice2;
	return totalsum;

}

int payout(int& gold,int &betting) {
	int playerDice = diceRoll();
	int AiDice = diceRoll();
    cout << "You rolled a total of " << playerDice << " while the opponent rolled a total of " << AiDice << endl;
	system("pause");
	if (playerDice == AiDice) {
		cout << "DRAW!!" << endl;
	}
	else {
		if (playerDice == 2) {
			cout << "SNAKE EYES" << endl;
			cout << "You Win" << endl;
			cout << "You won thrice your bet" << endl;
			cout << "You won " << betting * 3 << " gold" << endl;
			gold += (betting * 3);
		}
		else if (AiDice==2) {
			cout << "SNAKE EYES" << endl;
			cout << "You Lose" << endl;
			cout << "You got nothing" << endl;
		}
		else if (playerDice<AiDice) {
			cout << "You Lose" << endl;
			cout << "You got nothing" << endl;
		}
		else if (playerDice > AiDice) {
			cout << "You Win" << endl;
			cout << "You won " << betting * 2 << " gold" << endl;
			gold += (betting * 2);
		}
	}
	return gold;
}

int playround(int &gold) {
	int betting;
	cout << "Gold remaining: " << gold << endl;
	bet(gold, betting);
	payout(gold, betting);
	system("pause");
	system("cls");
	return gold;
}

int main() {
	srand(time(NULL));
	int gold = 1000;
	while (gold > 0) {
		playround(gold);
	}
	cout << "You have no more gold remaining" << endl;
	system("pause");
	return 0;
}