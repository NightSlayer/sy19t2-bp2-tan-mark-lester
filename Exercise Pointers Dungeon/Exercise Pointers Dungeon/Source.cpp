#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

void printInventory(vector<Item*> items)
{
	cout << "INVENTORY: " << endl;
	cout << "=========" << endl;
	int numberOfItems = items.size();
	for (int i = 0; i < numberOfItems; i++)
	{
		cout << items[i]->name << " | " << items[i]->gold << endl;
	}
	cout << "=========" << endl;
	cout << endl;
}

Item* generateRandomItem()
{
	int r = rand() % 5;
	Item* item = new Item;
	switch (r)
	{
	case 0:
		item->name = "Cursed Stone";
		item->gold = 0;
		break;
	case 1:
		item->name = "Mithril Ore";
		item->gold = 100;
		break;
	case 2:
		item->name = "Sharp Talon";
		item->gold = 50;
		break;
	case 3:
		item->name = "Thick Leather";
		item->gold = 25;
		break;
	case 4:
		item->name = "Jellopy";
		item->gold = 5;
		break;
	default:
		break;
	}

	return item;
}

int enterDungeon()
{
	bool willKeepLooting = true;
	vector<Item*> inventory;
	int multiplier = 1;
	int temporaryGold = 0;
	while (willKeepLooting) {
		// Get loot
		Item* loot = generateRandomItem();
		cout << "Current multiplier: " << multiplier << endl;
		cout << "Temporary gold: " << temporaryGold << endl;

		cout << "Looting... " << endl;
		system("pause");

		if (loot->name == "Cursed Stone")
		{
			cout << "Looted a Cursed Stone!" << endl;
			cout << "F" << endl;
			printInventory(inventory);
			for (int i = 0; i < inventory.size(); i++)
			{
				delete inventory[i];
			}
			return 0;
		}

		else
		{
			cout << "You got a " << loot->name << "!" << endl;

			int itemValue = loot->gold * multiplier;

			cout << "Item value: " << itemValue << endl;

			temporaryGold += itemValue;
			loot->gold = itemValue;
			inventory.push_back(loot);
			multiplier++;
			string input;
			cout << "Do you want to continue looting (y/n)?: ";
			cin >> input;
			if (input == "y")
			{
				willKeepLooting = true;
				system("cls");
			}
			else
			{
				willKeepLooting = false;
			}
		}

	}

	printInventory(inventory);
	for (int i = 0; i < inventory.size(); i++)
	{
		delete inventory[i];
	}
	return temporaryGold;
}

int main()
{
	srand(time(NULL));

	int playerGold = 50;
	int dungeonFee = 25;

	while (playerGold >= dungeonFee && playerGold < 500)
	{
		cout << "Player total gold: " << playerGold << endl;
		playerGold -= dungeonFee;

		cout << "Entering dungeon..." << endl;
		int earnedGold = enterDungeon();
		playerGold += earnedGold;
	}


	system("pause");
	return 0;
}
