#include <iostream>
#include <string>
#include <time.h>


using namespace std;

struct Item {
	string name;
	int goldvalue;
};



int main() {
	string names[15]{ "Leather", "Red Potion", "Blue Potion", "Sword", "Blaster", "Rune", "Bomb", "Bow", "Switch", "FirstAid", "Book", "Arrow", "Gun", "Bullet", "Core" };
	Item inventory[10];
	int sumValue = 0;
	for (int i = 0; i < 10; i++) {
		inventory[i] = { names[rand() % 15],(rand() % 100) + 1 };
	}
	for (int i = 0; i < 10; i++) {
		sumValue += inventory[i].goldvalue;
	}
	cout << "Inventory..." << endl;
	for (int i = 0; i < 10; i++) {
		cout << inventory[i].name << "\nValue: " << inventory[i].goldvalue << endl;
	}
	cout << "Total Value: " << sumValue << endl;
	system("pause");
	
}