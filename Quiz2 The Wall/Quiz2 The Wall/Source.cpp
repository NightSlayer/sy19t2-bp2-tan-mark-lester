#include <iostream>
#include <string>
#include <time.h>


using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};


/*The key members of the Night�s watch arrange themselves in a circle, with the exception of the Lord Commander.
The Lord Commander randomly selects one member in a circle as the starting point. This member is asked to hold the cloak of the Night�s watch.
The member who is holding the cloak randomly selects a number which range is equal to the number of members that are still in the game. (eg. If there are only 3 members left, possible draws are 1,2,3).
Based on the number rolled/selected, the cloak will be passed in a clockwise order. Rolling 5 means the cloak will be passed 5 times.
The member who holds the cloak after it was passed around will be eliminated. This member moves out of the circle and will stay to defend the Wall. The cloak will be passed to the member next to him (clockwise).
Repeat Step#3 until there�s only one member left. This member will take the cloak and move out to seek help.
*/


int main()
{
	srand(time(NULL));
	Node* n1 = new Node;
	Node* n2 = new Node;
	n1->next = n2;
	Node* n3 = new Node;
	n2->next = n3;
	Node* n4 = new Node;
	n3->next = n4;
    Node* n5 = new Node;
	n4->next = n5;
	n5->next = n1;
	
	n5->previous = n4;
	n4->previous = n3;
	n3->previous = n2;
	n2->previous = n1;
	n1->previous = n5;
	Node* current = n1;
	for (int i = 0; i < 5; i++)
	{
		cout << "Name for Soilder " << i + 1 << ": ";
		cin>> current->name;
		current = current->next;
	}
   
    int members = 5;
	while (members!=0) {
		int round = 1;
		Node* tobeDeleted=NULL;
        int randomRoll= rand() % members + 1;
		cout << "Round " << round << endl;
		cout << "Remaining Members" << endl;
		for (int i = 0; i < members; i++)
		{
			cout << current->name << endl;
			current = current->next;
		}
		cout << "Result" << endl;
		cout << current->name << " has drawn " << randomRoll << endl;
		for (int i = 0; i < randomRoll; i++)
		{
			current = current->next;
			tobeDeleted = current;
			
		}
		cout << tobeDeleted->name<<" has been eliminated" << endl;
	}
	
	system("pause");
	return 0;
}