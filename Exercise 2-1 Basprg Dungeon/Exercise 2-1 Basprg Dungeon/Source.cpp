#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

struct Item {
	string name;
	int goldvalue;
};


void CursedStone(Item RandomLoot, int money, int multiplier, int moneylooted) {
	vector<Item> itemLooted;
	itemLooted.push_back[RandomLoot];
	cout << "You got a Cursed Stone \nYou Died!! \nAll loot will be lost" << endl;
	printLootedItems(itemLooted, money, multiplier);
}

void printLootedItems(vector<Item> itemLooted, int money, int multiplier) {
	cout << "Item Looted" << endl;
	for (int i = 0; i < itemLooted.size; i++) {
		cout << itemLooted[i].name << "  " << itemLooted[i].goldvalue << endl;
	}
	money -= 25;
	EnterDungeon(money, multiplier);
}

Item RandomLoot(int money, int multiplier) {
	Item loot[5];
	loot[0] = { "Mithril Ore", 100 };
	loot[1] = { "Sharp Talon", 50 };
	loot[2] = { "Thick Leather", 25 };
	loot[3] = { "Jellopy", 5 };
	loot[4] = { "Cursed Stone", 0 };
	int moneylooted = 0;
	int i = rand() % 4;
	Item randomLoot = { loot[i] };
	switch (i) {
	  case 4:
		  CursedStone(randomLoot, money, multiplier, moneylooted);
		  break;
	  default:
          return randomLoot;
		  break;
	}
	
	
}

string confirm(vector<Item> itemLooted, int money, int multiplier) {
	char x;
	cout << "Want to continue looting?  y/n";
	cin >> x;
	switch (x) {
	case 'y':
		multiplier++;
		EnterDungeon(money, multiplier);
		break;
	case 'n':
		printLootedItems(itemLooted, money, multiplier);
	default:
		confirm(itemLooted, money, multiplier);
		cout << "Wrong Input" << endl;
		break;
	}
}

int EnterDungeon(int money, int multiplier) {
	int moneylooted;
	vector<Item> itemLooted;
	cout << "Money Left: " << money << "\nMultipler: " << multiplier << "\nLooting..." << endl;
	itemLooted.push_back(RandomLoot(money, multiplier));
	confirm(itemLooted, money, multiplier);

}


/*
  Loot	  Mithril Ore   Sharp Talon	Thick Leather	Jellopy		Cursed Stone
  Gold		100				50			25				5			0

  switch()
  }
  case:
   break;
  }
  default:
  }



*/

int main() {
	int gold = 50;
	int multiplier = 1;
	EnterDungeon(gold, multiplier);
	system("pause");
	
}